<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>Visits</h1>
            @foreach($urls as $url)
                @foreach($url->ipDatas as $ipData)
                    <li><strong>Url: </strong>{{$url->url}} | <strong>Ip: </strong>{{$ipData->ip}} | <strong>Accessed: </strong>{{$url->created_at/*$url->ipDatas()->where('url_id', $url->id)->first()->pivot->created_at*/}}</li>
                @endforeach
            @endforeach
        </div>
    </div>
</div>
</body>
</html>
