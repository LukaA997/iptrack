<?php

namespace App\Jobs;

use App\Models\IpData;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class GeoApi implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $ip;
    protected $urlId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($ip, $urlId)
    {
        $this->ip = $ip;
        $this->urlId = $urlId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $newIpData = Http::get('http://api.ipstack.com/'.$this->ip.'?access_key=e5961c3f46ce69ba13222dc59a63384d');
        $json = $newIpData->json();
        $ipData = new IpData;
        $ipData->fill([
            'ip' => $json['ip'],
            'country' => $json['country_name'],
            'city' => $json['city'],
            'zip_code' => $json['zip'],
        ]);
        $ipData->save();
        $ipData->userAccesses()->attach($this->urlId);
    }
}
