<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class IpData extends Model
{
    use HasFactory;

    protected $fillable = [
        'ip',
        'country',
        'city',
        'zip_code',
    ];

    public function userAccesses(): BelongsToMany
    {
        return $this->belongsToMany(UserAccess::class, 'access_ip', 'ip_id', 'url_id');
    }
}
