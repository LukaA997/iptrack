<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class UserAccess extends Model
{
    use HasFactory;

    protected $fillable = [
        'url',
    ];

    public function ipDatas(): BelongsToMany
    {
        return $this->belongsToMany(IpData::class, 'access_ip', 'url_id', 'ip_id')->withPivot('created_at');
    }
}
