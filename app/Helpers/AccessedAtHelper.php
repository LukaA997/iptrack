<?php

namespace App\Helpers;

use App\Models\UserAccess;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AccessedAtHelper
{
    public function test(UserAccess $userAccess)
    {
        return $userAccess->ipDatas()->where('url_id', $userAccess->id)->first()->pivot->created_at = Carbon::now();
    }
}
