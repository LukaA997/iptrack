<?php

namespace App\Http\Middleware;

use App\Jobs\GeoApi;
use App\Models\IpData;
use App\Models\UserAccess;
use Closure;
use Illuminate\Http\Request;

class StoreIp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $randomIps = ['104.119.251.150','57.225.153.150','82.138.37.156','84.122.191.228','73.140.82.158'];
        $url = url()->current();
        $ip = array_rand($randomIps);
        $userAccess = new UserAccess;
        $userAccess->fill([
            'url' => $url,
        ]);
        $userAccess->save();
        $ipData = IpData::where('ip', $randomIps[$ip])->first();
        if($ipData){
            $ipData->userAccesses()->attach($userAccess);
        }
        else{
            GeoApi::dispatch($randomIps[$ip], $userAccess);
        }
        return $next($request);
    }
}
