<?php

namespace App\Http\Controllers;

use App\Models\UserAccess;
use Illuminate\Http\Request;

class VisitorController extends Controller
{
    public function visitorStats(){
        $urls = UserAccess::all();
        return view('visitor')->with('urls', $urls);
    }
}
