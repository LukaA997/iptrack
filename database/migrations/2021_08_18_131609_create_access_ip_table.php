<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessIpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_ip', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ip_id');
            $table->unsignedBigInteger('url_id');
            $table->foreign('ip_id')->references('id')->on('ip_data');
            $table->foreign('url_id')->references('id')->on('user_accesses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_ip');
    }
}
